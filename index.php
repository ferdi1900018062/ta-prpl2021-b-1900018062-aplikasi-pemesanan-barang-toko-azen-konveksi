<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>AZEN KONVEKSI</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/css/s.css" rel="stylesheet">
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>
  
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">
      <h1 class="logo me-auto"><a href="index.php">AZEN KONVEKSI</a></h1>
  </header>
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h1>Selamat Datang </h1>
          <h1>di Azen Konveksi Online Store</h1>
          <h2>rasakan pengalaman belanja yang belum pernah anda <br> rasakan sebelumnya.</h2>
          <div class="d-lg-flex">
          <div class="agile-login">
      <ul>
      <?php
      if(!isset($_SESSION['log'])){
        echo '
        <a href="loginu.php" class="btn-get-started scrollto">Masuk sebagai admin</a>
        <a href="logins.php" class="btn-get-started scrollto">Masuk sebagai user</a>
        ';
      } else { 
        
        if($_SESSION['role']=='Member'){
        echo '
        <li style="color:white">Halo, '.$_SESSION["name"].'
        <li><a href="logout.php">Keluar?</a></li>
        ';
        } else {
        echo '
        <li style="color:white">Halo, '.$_SESSION["name"].'
        <li><a href="admin">Admin Panel</a></li>
        <li><a href="logout.php">Keluar?</a></li>
        ';
        };
        
      }
      ?>
      </ul>
    </div>
          </div>
           
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="assets/img/1610640310864.png" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>
  </section>
  <main id="main">

  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/js/main.js"></script>

</body>

</html>